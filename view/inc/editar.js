$(document).ready(function () {

    // ----- VÁRIAVEIS -----
    let btnAtualizar = $("#btnAtualizar");

    var full_url = document.URL; 
    var url_array = full_url.split('=') //Divida a string em uma matriz com / como separador até o id
    var idProduto = url_array[url_array.length-1];  // Pegue a última parte do array    (-1)

    
    // ----- MASK -----
    

    $('#inpPrecoProduto').bind('keypress',mask.money)


    // ----- CONTROLLER PRODUTO -----
    let controller = "/teste/controller/ControllerProduto.php";

    //PAGE ON LOAD
    buscarDadosProdutos(idProduto);

    //BUSCAR DADOS DO PRODUTO
    function buscarDadosProdutos(idProduto){
        $.ajax({
            url: controller,
            type: 'POST',
            data: {
                'action' : "busacarDadosProdutosAction",
                'idProduto' : idProduto
            },
            dataType: 'json',
            beforeSend: function() {
                try 
                {

                } catch (ex) {
                    alert("AJAX Error (beforeSend):\n" + ex);
                    console.error("AJAX Error (beforeSend):\n" + ex);
                }
            },
            // EXPECTED MODEL = { "error": false, "result": true }
            success: function(response) {
                console.log(response)    
                try 
                {
                    
                    $("#inpNomeProduto").val(response[0].nome)
                    $("#inpPrecoProduto").val(response[0].preco)
                    
                } catch (ex) 
                {
                    //swal("Erro!" , "AJAX Error (success):\n" + ex, 'error');
                    alert("AJAX Error (success):\n" + ex);
                }

            },
            error: function(error) {
                try 
                {
                    console.error(error)
                    alert("PHP Error (error):\n" + JSON.stringify(error));

                } catch (ex) 
                {
                    
                    console.error(ex)
                    alert("AJAX Error (error):\n" + JSON.stringify(ex));
                }

            }
        }); 
    }


       
    // ----- CADASTRAR PRODUTO / MANDA PARA CONTROLLER -----
    btnAtualizar.unbind().click(function(event) {     
        if($("#inpNomeProduto").val() == ''){
            alert("Nome do Produto Obrigatorio")
        }
        else if($("#inpNomeProduto").val() == ''){
            alert("Preço do Produto Obrigatorio")
        }
        else{
            
            $.ajax({
                url: controller,
                type: 'POST',
                data: {
                    'action' : "atualizarProdutoAction",
                    'inpNomeProduto' : $("#inpNomeProduto").val(),
                    'inpPrecoProduto' : $("#inpPrecoProduto").val(),
                    'idProduto': idProduto
                },
                dataType: 'json',
                beforeSend: function() {
                    try 
                    {

                    } catch (ex) {
                        alert("AJAX Error (beforeSend):\n" + ex);
                        console.error("AJAX Error (beforeSend):\n" + ex);
                    }
                },
                // EXPECTED MODEL = { "error": false, "result": true }
                success: function(response) {
                    console.log(response);
                    try 
                    {
                        if(response.result)
                        {
                            alert("Produto Atualizado com Sucesso");
                        }
                        else
                        {                            
                            alert("Erro ao  Atualizado Produto");
                        }
                    } catch (ex) 
                    {
                        //swal("Erro!" , "AJAX Error (success):\n" + ex, 'error');
                        alert("AJAX Error (success):\n" + ex);
                    }
    
                },
                error: function(error) {
                    try 
                    {
                        console.error(error)
                        alert("PHP Error (error):\n" + JSON.stringify(error));

                    } catch (ex) 
                    {
                        
                        console.error(ex)
                        alert("AJAX Error (error):\n" + JSON.stringify(ex));
                    }
    
                }
            }); 
        }

    });


   
    
});