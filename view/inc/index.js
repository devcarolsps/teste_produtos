$(document).ready(function () {

    // ----- VÁRIAVEIS -----
    let btnExcluir = "btnExcluir";
    // ----- CONTROLLER PRODUTO -----
    let controller = "/teste/controller/ControllerProduto.php";

    //PAGE ON LOAD
    listaProdutos();

    //CARREGA TODOS OS PRODUTOS
    function listaProdutos(){
        $.ajax({
            url: controller,
            type: 'POST',
            data: {
                'action' : "listarProdutosAction"
            },
            dataType: 'json',
            beforeSend: function() {
                try 
                {

                } catch (ex) {
                    alert("AJAX Error (beforeSend):\n" + ex);
                    console.error("AJAX Error (beforeSend):\n" + ex);
                }
            },
            // EXPECTED MODEL = { "error": false, "result": true }
            success: function(response) {
                try 
                {
                    var tabela = document.getElementById('tblCompleta');
                    response.forEach(function (obj) {
                        var tr = document.createElement('tr');
                        Object.keys(obj).forEach(function (chave) {
                            var td = document.createElement('td');
                            td.innerHTML = obj[chave];
                            console.log(tr)
                            tr.appendChild(td);
                            
                        });
                        tabela.appendChild(tr);
                    });       

                    
                } catch (ex) 
                {
                    //swal("Erro!" , "AJAX Error (success):\n" + ex, 'error');
                    alert("AJAX Error (success):\n" + ex);
                }

            },
            error: function(error) {
                try 
                {
                    console.error(error)
                    alert("PHP Error (error):\n" + JSON.stringify(error));

                } catch (ex) 
                {
                    
                    console.error(ex)
                    alert("AJAX Error (error):\n" + JSON.stringify(ex));
                }

            }
        }); 
    }



    //EXCLUIR PRODUTO
    $("#tblCompleta").on('click', '.' + btnExcluir, function(event){
        let idProduto = $(this).closest("tr").find('td').eq(0).text();
        var confirmado = confirm('Deseja deletar?');
        if(confirmado){
            $.ajax({
                url: controller,
                type: 'POST',
                data: {
                    'action' : "excluirProdutoAction",
                    'idProduto': idProduto
                },
                dataType: 'json',
                beforeSend: function() {
                    try 
                    {
    
                    } catch (ex) {
                        alert("AJAX Error (beforeSend):\n" + ex);
                        console.error("AJAX Error (beforeSend):\n" + ex);
                    }
                },
                // EXPECTED MODEL = { "error": false, "result": true }
                success: function(response) {
                    console.log(response);
                    try 
                    {
                        if(response.result)
                        {
                            alert("Produto Deletado com Sucesso");
                        }
                        else
                        {                            
                            alert("Erro ao  Deletado Produto");
                        }
                        
                        window.location.href="http://localhost/teste/view/";
                    } catch (ex) 
                    {
                        //swal("Erro!" , "AJAX Error (success):\n" + ex, 'error');
                        alert("AJAX Error (success):\n" + ex);
                    }
    
                },
                error: function(error) {
                    try 
                    {
                        console.error(error)
                        alert("PHP Error (error):\n" + JSON.stringify(error));
    
                    } catch (ex) 
                    {
                        
                        console.error(ex)
                        alert("AJAX Error (error):\n" + JSON.stringify(ex));
                    }
    
                }
            }); 
        }else{
            alert("Cancelado pedido de exclusão");
        }

        
        
    });



});