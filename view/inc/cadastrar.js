$(document).ready(function () {

    // ----- VÁRIAVEIS -----
    let btnCadastrar = $("#btnCadastrar");

    // ----- MASK -----    

    $('#inpPrecoProduto').bind('keypress',mask.money)

    // ----- CONTROLLER PRODUTO -----
    let controller = "/teste/controller/ControllerProduto.php";

       
    // ----- CADASTRAR PRODUTO / MANDA PARA CONTROLLER -----
    btnCadastrar.unbind().click(function(event) {     
        if($("#inpNomeProduto").val() == ''){
            alert("Nome do Produto Obrigatorio")
        }
        else{
            
            $.ajax({
                url: controller,
                type: 'POST',
                data: {
                    'action' : "cadastroProdutoAction",
                    'inpNomeProduto' : $("#inpNomeProduto").val(),
                    'inpPrecoProduto' : $("#inpPrecoProduto").val(),
                    'inpColorProduct' : $("#inpColorProduct").val(),
                },
                dataType: 'json',
                beforeSend: function() {
                    try 
                    {

                    } catch (ex) {
                        alert("AJAX Error (beforeSend):\n" + ex);
                        console.error("AJAX Error (beforeSend):\n" + ex);
                    }
                },
                // EXPECTED MODEL = { "error": false, "result": true }
                success: function(response) {
                    console.log(response);
                    try 
                    {
                        if(response.result)
                        {
                            alert("Produto Cadastado com Sucesso");
                            $("#inpNomeProduto").val('')
                            $("#inpPrecoProduto").val('')
                            $("#inpColorProduct").val('')
                        }
                        else
                        {                            
                            alert("Erro ao  Cadastadar Produto");
                        }
                    } catch (ex) 
                    {
                        //swal("Erro!" , "AJAX Error (success):\n" + ex, 'error');
                        alert("AJAX Error (success):\n" + ex);
                    }
    
                },
                error: function(error) {
                    try 
                    {
                        console.error(error)
                        alert("PHP Error (error):\n" + JSON.stringify(error));

                    } catch (ex) 
                    {
                        
                        console.error(ex)
                        alert("AJAX Error (error):\n" + JSON.stringify(ex));
                    }
    
                }
            }); 
        }

    });


    
});