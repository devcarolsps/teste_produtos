<!DOCTYPE HTML>
<html>
<?php include("head.php") ?>
<script src="inc/functions.js"></script>
<script src="inc/editar.js"></script>
<body>
    <?php include("menu.php") ?>
    <div class="container">
        <div class="col-12 border-bottom mb-5">
            <h2>Atualizar Produto</h2>
        </div>
        <div class="row">
            <form class="col-10" id="frmCadastrado">
                <div class="form-group mr-1">
                    <label>Nome Produto</label>
                    <input type="text" class="form-control" placeholder="Nome Produto..." id="inpNomeProduto" name="inpNomeProduto">
                </div>


                <div class="form-group mr-1">
                    <label>Preço Produto</label>
                    <input type="text" class="form-control" placeholder="Preço Produto..." id="inpPrecoProduto" name="inpPrecoProduto">
                </div>

                <div class="form-group">
                    <a class="btn btn-success" id="btnAtualizar">Atualizar Produto</a>
                </div>
            </form>
        </div>
    </div>
</body>

</html>
