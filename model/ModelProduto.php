<?php
require_once("../Data/DAO.php");

class Produto extends Banco {

    private $idprod;
    private $nome;
    private $preco;
    private $cor;

    //Metodos Set
    public function setId($string){
        $this->idprod = $string;
    }

    public function setNome($string){
        $this->nome = $string;
    }
    public function setPreco($string){
        $this->preco = $string;
    }
    public function setCor($string){
        $this->cor = $string;
    }

    //Metodos Get
    
    public function getId(){
        return $this->idprod;
    }

    public function getNome(){
        return $this->nome;
    }

    public function getPreco(){
        return $this->preco;
    }
    public function getCor(){
        return $this->cor;
    }


    public function insert(){
        return $this->setProduto($this->getNome(),$this->getPreco(),$this->getCor());
    }

    public function listarProdutos(){
        return $this->listProdutos();
    }

    public function buscarDadosProduto(){
        return $this->getProduto($this->getId());
    }

    public function atualizarProduto(){
        return $this->updateProduto($this->getNome(),$this->getPreco(),$this->getId());
    }

    public function deletarProduto(){
        return $this->delete($this->getId());
    }
}
?>
