<?php

require_once(__DIR__.'/../model/ModelProduto.php');


// RESULT VARIABLE PROPERTIES
$errorProp = "error";
$resultProp = "result";

// DEFAULT RESULT VARIABLE
$result[$errorProp] = "Nenhuma ação obtida";
$result[$resultProp] = null;

// DEFAULT ERROR WHEN NO ACTION WAS TRIGGERED
$noTriggeredError = "Nenhuma ação foi acionada";


if( (isset($_POST['action'])  ))
{
    try
    {
        if( $_POST['action'] == "cadastroProdutoAction")  
        {
            
            
            header('Content-Type: application/json');
            
            $newId = 0;

            $produto = new Produto();
            
            $produto->setNome($_POST['inpNomeProduto']);
            $produto->setPreco($_POST['inpPrecoProduto']);
            $produto->setCor($_POST['inpColorProduct']);           

            $resultado = $produto->insert();        
            
            if($resultado){
                $result[$errorProp] = false;
                $result[$resultProp] = true;
            }else{                
                $result[$errorProp] = true;
                $result[$resultProp] = false;
            }
            

            print_r(json_encode($result));     // EXPECTED MODEL = { "error": false, "result": true }*/
            
        }
        else if($_POST['action'] == "listarProdutosAction")
        {
            
            header('Content-Type: application/json');
            
            $produto = new Produto();
            
            $result = $produto->listarProdutos();       

            print_r(json_encode($result));     // EXPECTED MODEL = { "error": false, "result": true }*/
            
        }
        else if($_POST['action'] == "busacarDadosProdutosAction")
        {
            
            header('Content-Type: application/json');
            
            $idProduto = $_POST['idProduto'];

            $produto = new Produto();
            
            $produto->setId($idProduto);
            $result = $produto->buscarDadosProduto();       

            print_r(json_encode($result));     // EXPECTED MODEL = { "error": false, "result": true }*/
            
        }
        else if( $_POST['action'] == "atualizarProdutoAction")  
        {
            
            
            header('Content-Type: application/json');
            
            $produto = new Produto();
            
            $produto->setNome($_POST['inpNomeProduto']);
            $preco = $_POST['inpPrecoProduto'];
            $produto->setPreco($preco); 

            $produto->setId($_POST['idProduto']);     

            $resultado = $produto->atualizarProduto();        
            
            if($resultado){
                $result[$errorProp] = false;
                $result[$resultProp] = true;
            }else{                
                $result[$errorProp] = true;
                $result[$resultProp] = false;
            }
            

            print_r(json_encode($result));     // EXPECTED MODEL = { "error": false, "result": true }*/
            
        }        
        else if( $_POST['action'] == "excluirProdutoAction")  
        {
            
            header('Content-Type: application/json');

            $produto = new Produto();
            
            $produto->setId($_POST['idProduto']);     

            $resultado = $produto->deletarProduto();        
            
            if($resultado){
                $result[$errorProp] = false;
                $result[$resultProp] = true;
            }else{                
                $result[$errorProp] = true;
                $result[$resultProp] = false;
            }
            

            print_r(json_encode($result));     // EXPECTED MODEL = { "error": false, "result": true }*/
            
        }
        
        else
        {
            header('Content-Type: application/json');

            $result[$resultProp] = null;
            $result[$errorProp] = $noTriggeredError;
            
            print_r(json_encode($result));   // DEFAULT = { "error": "Nenhuma ação foi acionada", "result": false }
        }
    }
    catch (Exception $exception)
    {
        header('Content-Type: application/json');

       // $erro = $exception->getMessage();

        $result[$resultProp] = null;
        $result[$errorProp] = 'Erro';

        print_r(json_encode($result));   // EXPECTED MODEL = { "error": "Exemplo de erro!", "result": false }
    }
}
else
{
    header('Content-Type: application/json');

    print_r(json_encode($result));   // DEFAULT = { "error": "Nenhuma ação obtida", "result": false }
}