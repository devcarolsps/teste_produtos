<?php

require_once("init.php");
class Banco{

    protected $mysqli;

    public function __construct(){
        $this->conexao();
    }

    private function conexao(){
        $this->mysqli = new mysqli(BD_SERVIDOR, BD_USUARIO , BD_SENHA, BD_BANCO);
    }

    public function setProduto($nome,$preco,$cor){
        $query = "INSERT INTO produtos (nome, cor, preco) VALUES (?,?,?)";
        $stmt = $this->mysqli->prepare($query);
        $stmt->bind_param("sss",$nome,$cor,$preco);
         if($stmt->execute()){
            return true ;
        }else{
            return false;
        }

    }

    public function listProdutos(){
        $result = $this->mysqli->query("SELECT * FROM produtos ORDER BY idprod");

        $array = [];
        while($row = $result->fetch_array(MYSQLI_ASSOC)){
            $jsonOBJ = [];
            $jsonOBJ['idprod'] = $row['idprod'];
            $jsonOBJ['nome'] = $row['nome'];
            $jsonOBJ['cor'] = '<input value="'.$row["cor"].'" type="text" disabled style="background-color: '.$row["cor"].'; "  />'; //$row['cor'];
            $jsonOBJ['preco'] = number_format($row['preco'],2,",",".");

            $desconto20 = 20.0 / 100.0; // Produtos das cores AZUL e VERMELHO, Terão um desconto de 20%.
            $desconto10 = 10.0 / 100.0; // Produtos das cores AMARELHO, Terão um desconto de 10%.
            $desconto5 = 5.0 / 100.0; // Produtos das cores AMARELHO, Terão um desconto de 5%.
            $desconto25 = 25.0 / 100.0; // 25% 20 = vermelho e 5 = maior que 50

            if( ($row['cor'] === '#ff0000') && ( $row['preco'] > 50 ) ) {     
                   
                $precoComDesconto = number_format($row['preco'] - ($desconto25 * $row['preco']),2,",",".");  
            }
            else if( ($row['cor'] == '#0000ff') || ($row['cor'] == '#ff0000') ){
                $precoComDesconto = number_format($row['preco'] - ($desconto20 * $row['preco']),2,",","."); 

            }
            else if($row['cor'] == '#ffff00') {

                $precoComDesconto = number_format($row['preco'] - ($desconto10 * $row['preco']),2,",",".");

            }
            else{
                $precoComDesconto = 0;
            }
           
            $jsonOBJ['preco_desconto'] = $precoComDesconto;
            $jsonOBJ['acao'] = '<a href="editar.php?id='.$row["idprod"].' " class="btn btn-sm btn-success">Atualizar</a>';
            $jsonOBJ['acao'] .= '<a  class="btn btn-sm btn-danger btnExcluir" >Excluir</a>';

            
            array_push($array,$jsonOBJ);


        }
        return $array;

    }

    public function getProduto($idprod){
        $result = $this->mysqli->query("SELECT * FROM produtos WHERE idprod = $idprod ORDER BY idprod");

        $array = [];
        while($row = $result->fetch_array(MYSQLI_ASSOC)){
            $jsonOBJ = [];
            $jsonOBJ['idprod'] = $row['idprod'];
            $jsonOBJ['nome'] = $row['nome'];
            $jsonOBJ['preco'] = $row['preco'];
            
            array_push($array,$jsonOBJ);


        }
        return $array;

    }

    public function delete($idprod){
        if($this->mysqli->query("DELETE FROM produtos WHERE idprod = '".$idprod."';")== TRUE){
            return true;
        }else{
            return false;
        }

    }
    public function pesquisaLivro($id){
        $result = $this->mysqli->query("SELECT * FROM livros WHERE nome='$id'");
        return $result->fetch_array(MYSQLI_ASSOC);

    }
    public function updateProduto($nome,$preco,$id){
        $stmt = $this->mysqli->prepare("UPDATE produtos SET nome = ?, preco = ? WHERE idprod = ?");
        $stmt->bind_param("sss",$nome,$preco,$id);
        if($stmt->execute()==TRUE){
            return true;
        }else{
            return false;
        }
    }
}
?>
